package mongodb;

import java.util.Scanner;

import org.bson.Document;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;


public class MongoDBConnection {

	public static void main(String[] args) {
		
		MongoClient client = MongoClients.create("mongodb://localhost");
		MongoDatabase db = client.getDatabase("friends");
		MongoCollection<Document> collection = db.getCollection("samples_friends");

		
		Scanner keyboardInput = new Scanner(System.in);
		
		// TODO Auto-generated method stub

		System.out.print("Type the season to list the data: ");
		int season = keyboardInput.nextInt();
		
		while(season > 0)
		{
			Iterable<Document> d = collection.find(new Document("season", season));
			if(d.iterator().hasNext())
			{
				for(Document d1 : d) {
					System.out.println("Season " + season + " : Episode " + d1.get("number") + " " + d1.get("name"));	
				}
			}
			else {
				System.out.println("Invalid Season!");
			}
			
			System.out.print("\nType the season to list the data (-1 to exit): ");
			season = keyboardInput.nextInt();
		}
		
		
		
		client.close();
	}

}
